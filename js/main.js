var idSelected = null;
var BASE_URL = "https://643a58b8bd3623f1b9b16433.mockapi.io/sv";
// lấy dữ liệu từ web api về => render ra
function fetch() {
  turnOnLoading();
  serviceSv
    .getList()
    .then(function (res) {
      render(res.data.reverse());
      turnOffLoading();
    })
    .catch(function (err) {});
}
fetch();
function xoaSV(id) {
  serviceSv
    .remove(id)
    .then(function (res) {
      fetch();
      turnOnLoading();
    })
    .catch(function (err) {});
  turnOnLoading();
}
function suaSV(id) {
  document.getElementById("txtMaSV").disabled = true;
  turnOnLoading();
  idSelected = id;
  serviceSv
    .edit(id)
    .then(function (res) {
      showThongTinLenForm(res.data);
      turnOffLoading();
    })
    .catch(function (err) {});
}
function themSinhVien() {
  turnOnLoading();
  serviceSv
    .add()
    .then(function (res) {
      fetch();
      Toastify({
        text: "Thêm sinh viên thành công",
        duration: 3000,
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
      }).showToast();
    })
    .catch(function (err) {});
  // turnOffLoading();
}
function capNhatSinhVien() {
  document.getElementById("txtMaSV").disabled = false;
  serviceSv.update()
    .then(function (res){
      fetch();
    })
    .catch(function (err) {});
}
function btnReset() {
  document.getElementById("formQLSV").reset();
}
