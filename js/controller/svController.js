function render(x) {
  var contentHTML = "";
  for (var i = 0; i < x.length; i++) {
    sv = x[i];
    var contentTr = ` <tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>0</td>
    <td> <button onclick="xoaSV(${sv.ma})" class="btn btn-danger">Delete</button> 
         <button  onclick="suaSV(${sv.ma})"class="btn btn-success">Edit</button> 
    </td>
  </tr> `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;
  var sv = new SinhVien(ma, ten, email, matKhau, toan, ly, hoa);
  return sv;
}
function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}
/*
  _                 _ _             
 | |               | (_)            
 | | ___   __ _  __| |_ _ __   __ _ 
 | |/ _ \ / _` |/ _` | | '_ \ / _` |
 | | (_) | (_| | (_| | | | | | (_| |
 |_|\___/ \__,_|\__,_|_|_| |_|\__, |
                               __/ |
                              |___/ 
 */
var turnOnLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var turnOffLoading = function () {
  document.getElementById("loading").style.display = "none";
};

var serviceSv = {
  getList: function () {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },

  remove: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },

  edit: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },

  add: function () {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: layThongTinTuForm(),
    });
  },

  update:function(){
   return axios({
      url: `${BASE_URL}/${idSelected}`,
      method: "PUT",
      data: layThongTinTuForm()
    })
  }
};
